interface IStudent {
  firstname: string;
  lastname: string;
  getFirstname: () => string;
  getLastname: () => string;
}

type studentProps = {
  firstname: string;
  lastname: string;
};

class Student implements IStudent {
  public firstname: string;
  public lastname: string;

  constructor(props: studentProps) {
    this.firstname = props.firstname;
    this.lastname = props.lastname;
  }
    public getFirstname(): string {
        return `Nom : ${this.firstname}`;
    }
    public getLastname(): string {
    return `Prénom : ${this.lastname}`;
    };
}

interface ISkill {
  name: string;
  description: string;
  optionProp?: number;
  getSkill: () => string;
  getDescription: () => string;
}

type skillProps = {
  name: string;
  description: string;
  optionProp?: number;
};

class Skill implements ISkill {
  public name: string;
  public description: string;
  public optionProp?: number;

  constructor(props: skillProps) {
    this.name = props.name;
    this.description = props.description;
    this.optionProp = props.optionProp;
  }

  public getSkill(): string {
    return `Skill (compétence) : ${this.name}`;
  }

  public getDescription(): string {
    return `Description de la compétence : ${this.description}`;
  }
}

const skill1Props = {
  name: "Collaborer à la gestion...",
  description: "Collaborer à la gestion description ...",
};

const skill1: Skill = new Skill(skill1Props);

console.log(skill1.getSkill());
console.log(skill1.getDescription());

interface ISCategory {
  nameCategory: string;
  descriptionCategory: string;
  optionPropCategory?: number;
  getCategory: () => string;
  getDescriptionCategory: () => string;
}

type categoryProps = {
  nameCategory: string;
  descriptionCategory: string;
  optionPropCategory?: number;
};

class Category implements ISCategory {
  public nameCategory: string;
  public descriptionCategory: string;
  public optionPropCategory?: number;

  constructor(props: categoryProps) {
    this.nameCategory = props.nameCategory;
    this.descriptionCategory = props.descriptionCategory;
    this.optionPropCategory = props.optionPropCategory;
  }

  public getCategory(): string {
    return `Catégory : ${this.nameCategory}`;
  }

  public getDescriptionCategory(): string {
    return `Description : ${this.descriptionCategory}`;
  }
}

const category1Props = {
  nameCategory: "imiter",
  descriptionCategory: "Description de la categorie",
};

const category1: Category = new Category(category1Props);

console.log(category1.getCategory());
console.log(category1.getDescriptionCategory());

const student1Props = {
    firstname: "Hugo",
    lastname:"BARNAS"
}

const student1: Student = new Student(student1Props)

console.log(student1.getFirstname());
console.log(student1.getLastname())

